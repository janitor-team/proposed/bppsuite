bppsuite (2.4.1-3) unstable; urgency=medium

  * Approximate autopkgtest output. Closes: #963088.
  * Add Pranav Ballaney to Uploaders

 -- Pranav Ballaney <ballaneypranav@gmail.com>  Tue, 15 Sep 2020 18:56:51 +0530

bppsuite (2.4.1-2) unstable; urgency=medium

  * Team upload.

  [ Pranav Ballaney ]
  * Add reference data for autopkgtests
  * Install examples for bppsuite
  * Add autopkgtests
  * Install docs

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Refer to specific version of license GPL-2+.
  * Fix day-of-week for changelog entry 0.3.0.
  * Cleanup installation of manpages

 -- Pranav Ballaney <ballaneypranav@gmail.com>  Fri, 12 Jun 2020 06:00:09 +0530

bppsuite (2.4.1-1) unstable; urgency=medium

  [ Julien Dutheil ]
  * New upstream version

  [ Andreas Tille ]
  * Standards-Version: 4.2.0

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 22 Aug 2018 09:18:13 +0200

bppsuite (2.4.0-1) unstable; urgency=medium

  [ Julien Dutheil ]
  * New upstream version
  * Added full text of CeCILL license.
  * Compiles with -DCMAKE_BUILD_TYPE=RelWithDebInfo to generate debug info
    (lintian warning)
  * Point Vcs-fields to salsa.debian.org

  [ Andreas Tille ]
  * Standards-Version: 4.1.4

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Tue, 10 Apr 2018 14:53:00 +0200

bppsuite (2.3.2-1) unstable; urgency=medium

  [ Julien Dutheil ]
  * New upstream version

  [ Steffen Moeller ]
  * debian/upstream/metadata; added reference to registries

  [ Andreas Tille ]
  * Standards-Version: 4.1.3
  * debhelper 11

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 07 Feb 2018 11:28:09 +0200

bppsuite (2.3.1-4) unstable; urgency=medium

  * Add debhelper install file
  * Standards-Version: 4.1.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Tue, 12 Sep 2017 15:01:42 +0200

bppsuite (2.3.1-3) unstable; urgency=medium

  * Fixed linkage issue in x32 arch (second attempt)

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 19 Jul 2017 20:56:28 +0200

bppsuite (2.3.1-2) unstable; urgency=medium

  * Fixed linkage issue in x32 arch.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Tue, 18 Jul 2017 21:47:05 +0200

bppsuite (2.3.1-1) unstable; urgency=medium

  * New upstream version
  * Take over to Debian Med team maintenance
  * cme fix dpkg-control
  * debhelper 10
  * DEP5
  * Add watch file
  * d/rules: use dh
  * Fix manpage debhelper input
  * Adjust Build-Depends
  * Enhance description
  * Add examples package

 -- Andreas Tille <tille@debian.org>  Wed, 14 Jun 2017 15:12:35 +0200

bppsuite (2.2.0-0.1) unstable; urgency=low

  [ Andreas Beckmann ]
  * Non-maintainer upload.
  * New upstream release 2.2.0 (git commit 7890d12).
    (Closes: #776111, #825443, #708457)
  * Remove hardcoded library dependencies.  (Closes: #811271)

  [ Julien Dutheil ]
  * Compatibility update. Bio++ Program Suite version number is now indexed
    on Bio++'s version.
  * Programs support the --seed argument for setting the random seed.
  * bppSeqGen support generic characters as input.
  * bppPhySamp outputs sampled trees.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 10 Sep 2016 13:35:04 +0200

bppsuite (0.8.0-1+build1) unstable; urgency=low

  * New models for proteins (COaLA)
  * New program bppMixedLikelihoods

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Fri, 08 Mar 2013 11:41:00 +0100

bppsuite (0.7.0-1) unstable; urgency=low

  * Several program improvements (more models, options, etc.)
  * New program bpp Alignment scores.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 15 Feb 2012 09:17:00 +0100

bppsuite (0.6.2-1) unstable; urgency=low

  * RFP: Bio++ -- The Bio++ bioinformatics libraries. (Closes: #616373).
  * Packages are now non-native.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Thu, 09 Jun 2011 11:00:00 +0100

bppsuite (0.6.1) unstable; urgency=low

  * Compatibility update with bpp-phyl 2.0.1.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Mon, 28 Feb 2011 09:00:00 +0100

bppsuite (0.6.0) unstable; urgency=low

  * Compatibility update with Bio++ 2.0.0.
  * New mixed substitution models.
  * More sequence manipulation tools.
  * Several bug fixed and syntax improvements.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Mon, 07 Feb 2011 09:00:00 +0100

bppsuite (0.5.0) unstable; urgency=low

  * New substitution models.
  * New tree drawing program.
  * Compatibility update with Bio++ 1.9.0.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Thu, 25 Mar 2010 21:17:37 +0100

bppsuite (0.4.0) unstable; urgency=low

  * Major syntax update, using keyvals.
  * Compatibility update with Bio++ 1.8.0.

 -- Julien Dutheil <jdutheil@birc.au.dk>  Wed, 10 Jun 2009 11:28:58 +0100

bppsuite (0.3.1) unstable; urgency=low

  * Several bug fixed.

 -- Julien Dutheil <jdutheil@birc.au.dk>  Thu, 11 Dec 2008 12:21:37 +0100

bppsuite (0.3.0) unstable; urgency=low

  * Initial release

 -- Julien Dutheil <jdutheil@birc.au.dk>  Thu, 25 Sep 2008 14:28:21 +0200
